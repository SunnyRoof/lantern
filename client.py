import asyncio
import argparse
import time


class LengthError(Exception):
    """Exception for correct but incomplete message"""
    pass

class LanternClientProtocol(asyncio.Protocol):
    """
    Simple TCP client with command parsing
    """
    def __init__(self, loop):
        self.loop = loop
        self.buf = bytearray()
        self.commands = {
            0x12: self.turn_on,
            0x13: self.turn_off,
            0x20: self.change_color,
        }


    def connection_made(self, transport):
        print('connection established')


    def data_received(self, data):
        print('received', data)
        self.buf += bytearray(data)
        self.parse()


    def connection_lost(self, exc):
        print('server closed the connection')
        print(exc)
        self.loop.stop()


    def parse(self):
        try:
            while len(self.buf) > 0:
                candidate = self.buf[0]
                if candidate in self.commands:
                    self.commands[candidate]()
                else:
                    self.buf.pop(0)
        except LengthError:
            # insufficient length of buffer, wait for more data and try again
            print('length error')

    def turn_on(self):
        expected_length = 0
        if len(self.buf) >= 3:
            if int.from_bytes(self.buf[1:3], byteorder='big', signed=False) == expected_length:
                self.buf = self.buf[3:]
                print('ON')
            else:
                self.buf.pop(0)
        else:
            raise LengthError


    def turn_off(self):
        expected_length = 0
        if len(self.buf) >= 3:
            if int.from_bytes(self.buf[1:3], byteorder='big', signed=False) == expected_length:
                self.buf = self.buf[3:]
                print('OFF')
            else:
                self.buf.pop(0)
        else:
            raise LengthError


    def change_color(self):
        expected_length = 3
        if len(self.buf) >= 3:
            if int.from_bytes(self.buf[1:3], byteorder='big', signed=False) == expected_length:
                if len(self.buf) >= 3 + expected_length:
                    color = int.from_bytes(self.buf[3 : 3 + expected_length], byteorder='big', signed=False)
                    self.buf = self.buf[3 + expected_length:]
                    print('Color changed: ', hex(color))
                else:
                    raise LengthError
            else:
                self.buf.pop(0)
        else:
            raise LengthError


if __name__ == "__main__":
    connection_args = argparse.ArgumentParser()
    connection_args.add_argument("-i", default='127.0.0.1')
    connection_args.add_argument("-p", type=int, default=8888)
    connection_info = connection_args.parse_args()
    loop = asyncio.get_event_loop()
    while True:
        try:
            coro = loop.create_connection(lambda: LanternClientProtocol(loop),
                                          connection_info.i, connection_info.p)
            client = loop.run_until_complete(coro)
            loop.run_forever()

        except Exception as e:
            print(e)
            time.sleep(3)

        except KeyboardInterrupt:
            print('exit')
            break